from concurrent.futures import thread
from sqlalchemy import create_engine, MetaData, Table, select
from datetime import datetime
from multiprocessing import Pool


engine = create_engine("sqlite:///./database.db", connect_args={"check_same_thread": False})
metadata = MetaData()
# start = datetime.now()
with engine.connect() as conn: 
    tab_sodch = Table('SODCH', metadata, autoload_with=engine)
    col_sodch = [c.name for c in tab_sodch.columns]
    query = select([tab_sodch])
    cur = conn.execute(query)
    records = cur.fetchall()
    sodch = [dict(zip(col_sodch, rec))for rec in records]

# print(f'Все записи из  SODCH импортированы за {datetime.now() - start}')


# start = datetime.now()
with engine.connect() as conn:
    tab_dm = Table('DM', metadata, autoload_with=engine)
    col_dm = [c.name for c in tab_dm.columns]
    query = select([tab_dm])
    cur = conn.execute(query)
    records = cur.fetchall()
    dm = [dict(zip(col_dm, rec))for rec in records]
# print(f'Все записи из DM импортированы за {datetime.now() - start}')

def iter_dm(dch):
    for d in dm:
        if dch['phone'] in d['accout']:
            print(dch['phone'], d['accout'])

if __name__ == '__main__':
    start = datetime.now()
    with Pool(8) as p:
        p.map(iter_dm, sodch)

    print(f'Сравнение выполнено за {datetime.now() - start}')