from sqlalchemy import create_engine, MetaData, Column, Integer, String, Date, Table

# engine = create_engine("postgresql://postgres:postgres@localhost:5432/fackedata")
engine = create_engine("sqlite:///./database.db", connect_args={"check_same_thread": False})
metadata = MetaData()

sodch_table = Table(
    'SODCH',
    metadata,
    Column('id', Integer, primary_key=True),
    Column('kusp_num', String, nullable= False),
    Column('date_kusp', Date, nullable=False),
    Column('phone', String, nullable=False)
)

dm_table = Table(
    'DM',
    metadata,
    Column('id', Integer, primary_key=True),
    Column('accout', String, nullable=False)
)

with engine.begin() as conn:
    metadata.create_all(conn)

    for table in metadata.tables.keys():
        print (f'{table} successfully created')