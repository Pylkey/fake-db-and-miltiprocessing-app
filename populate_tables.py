from sqlalchemy import create_engine, MetaData
from faker import Faker
import sys, datetime

# engine = create_engine("postgresql://postgres:postgres@localhost:5432/fackedata")
engine = create_engine("sqlite:///./database.db", connect_args={"check_same_thread": False})
metadata = MetaData()

faker = Faker()

with engine.connect() as conn: 
    metadata.reflect(conn)

sodch = metadata.tables['SODCH']
dm = metadata.tables['DM']


class GenerateData:
    """
    Генерируем определенное количество записей в БД
    """
    def __init__(self):
        """
        Считываем аргументы команды
        """
        self.table = sys.argv[1]
        self.num_records = int(sys.argv[2])

    def create_data(self):
        """
        Используем faker для генерации данных и вставляем их в таблицу
        """
        
        if self.table not in metadata.tables.keys():
            return print(f"{self.table} does not exist")

        if self.table == 'SODCH':
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = sodch.insert().values(
                        kusp_num = faker.numerify(text='@@###'),
                        date_kusp = faker.date_between(start_date=datetime.date(2021,1,1), end_date=datetime.date(2021,12,31)),
                        phone = faker.numerify(text='8####')
                    )
                    conn.execute(insert_stmt)
    
        if self.table == 'DM':
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = dm.insert().values(
                        accout = faker.numerify(text='####'),
                    )
                    conn.execute(insert_stmt)

if __name__ == "__main__":
    generate_data = GenerateData()
    generate_data.create_data()